#!/bin/bash
#
# make-redirects.sh - Create backward compatibility redirects.
#
# The WinPcap web site was originally, or at least at one time, created with
# Microsoft FrontPage, which used "default.htm" as the default document name.
# For each default.htm add:
# /winpcapweb/default.htm /winpcapweb/
# to public/_redirects

# These are the original "default.htm"s, which have been renamed to index.html via
# for dhtm in $default_htms ; do git mv $dhtm ${dhtm/default.htm/index.html}; done
default_htms="
    default.htm
    docs/default.htm
    install/default.htm
    ntar/default.htm
    ntar/docs/default.htm
    ntar/download/default.htm
    windump/default.htm
    windump/docs/default.htm
    windump/install/default.htm
"

for default_htm in $default_htms; do
    printf "/%s /%s\\n" "$default_htm" "${default_htm/default.htm/}"
done