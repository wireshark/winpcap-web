From SKlein at hilscher.com  Thu Oct 10 08:14:38 2013
From: SKlein at hilscher.com (SKlein at hilscher.com)
Date: Thu, 10 Oct 2013 10:14:38 +0200
Subject: [Winpcap-users] New part for WinPCap developed - where can I submit
	it?
Message-ID: <3B0DA8FB62D6E74891DCC4019AA54CB3057FF44F@maiser01.hilscher.local>

Hi WinPCap Team,

I developed an interface to WinPCap for a new device called netANALYZER.

(http://hilscher.com/products_group_kits_NANL.html)

 

Where can I upload it to WinPCap? Is there a svn server where I can
commit it or should I send it via email?

I found nothing to this topic at winpcap.org/devel.





Hilscher Gesellschaft f?r Systemautomation mbH  |  Rheinstrasse 15  |  65795 Hattersheim  |  Germany  |  www.hilscher.com
Sitz der Gesellschaft / place of business: Hattersheim  |  Registergericht / register court: Amtsgericht Frankfurt/Main
Ust.-Nr.: / VAT No.: DE113852715  |  Handelsregister / commercial register: Frankfurt B 26873
Gesch?ftsf?hrer / managing director: Hans-J?rgen Hilscher

Important Information:
This e-mail message including its attachments contains confidential and legally protected information solely intended for the addressee. If you are not the intended addressee of this message, please contact the addresser immediately and delete this message including its attachments. The unauthorized dissemination, copying and change of this e-mail are strictly forbidden. The addresser shall not be liable for the content of such changed e-mails.

Wichtiger Hinweis:
Diese E-Mail einschlie?lich ihrer Anh?nge enth?lt vertrauliche und rechtlich gesch?tzte Informationen, die nur f?r den Adressaten bestimmt sind. Sollten Sie nicht der bezeichnete Adressat sein, so teilen Sie dies bitte dem Absender umgehend mit und l?schen Sie diese Nachricht und ihre Anh?nge. Die unbefugte Weitergabe, das Anfertigen von Kopien und jede Ver?nderung der E-Mail ist untersagt. Der Absender haftet nicht f?r Inhalte von ver?nderten E-Mails.


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.winpcap.org/pipermail/winpcap-users/attachments/20131010/4934ec67/attachment.html>

From member at linkedin.com  Mon Oct 21 02:01:40 2013
From: member at linkedin.com (=?UTF-8?Q?Pang-Hsin_Wang_=C3=BCber_LinkedIn?=)
Date: Mon, 21 Oct 2013 02:01:40 +0000 (UTC)
Subject: [Winpcap-users] Treten Sie meinem Netzwerk auf LinkedIn bei
Message-ID: <2129917455.13914549.1382320900046.JavaMail.app@ela4-app1201.prod>

LinkedIn
------------




    Pang-Hsin Wang m?chte Sie als Kontakt auf LinkedIn hinzuf?gen:
  

------------------------------------------

Ich m?chte Sie gerne zu meinem beruflichen Netzwerk auf LinkedIn hinzuf?gen.

Einladung von Pang-Hsin Wang annehmen
http://www.linkedin.com/e/-l878ib-hn1268qa-6j/hT6jN1l8EGd1lCBU2U6WI2NuydOPxEMHVLRYsg_/blk/I317610206_190/e39SrCAJoS5vrCAJoyRJtCVFnSRJrScJr6RBfnhv9ClRsDgZp6lQs6lzoQ5AomZIpn8_c3ANnPoMcz0NdzsNcQALcBZIhnpWcA8LdPkVej8NcjgQdP4LrCBxbOYWrSlI/eml-comm_invm-b-in_ac-inv28/?hs=false&tok=3FL9tW817pyBY1

Profil von Pang-Hsin Wang anzeigen
http://www.linkedin.com/e/-l878ib-hn1268qa-6j/rso/153468124/-jIf/name/190893447_I317610206_190/?hs=false&tok=2p8bQnjkrpyBY1
------------------------------------------
Sie erhalten Einladungs-E-Mails.


Diese E-Mail war an abc def gerichtet.
Erfahren Sie, warum wir dies hinzuf?gen: http://www.linkedin.com/e/-l878ib-hn1268qa-6j/plh/http%3A%2F%2Fhelp%2Elinkedin%2Ecom%2Fapp%2Fanswers%2Fdetail%2Fa_id%2F4788/-GXI/?hs=false&tok=2Wsiu5TA3pyBY1

(c) 2012, LinkedIn Corporation. 2029 Stierlin Ct., Mountain View, CA 94043, USA


  
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.winpcap.org/pipermail/winpcap-users/attachments/20131021/3a2178fc/attachment.html>

From jdhurst at myself.com  Wed Oct 23 17:17:17 2013
From: jdhurst at myself.com (John Hurst)
Date: Wed, 23 Oct 2013 13:17:17 -0400
Subject: [Winpcap-users] Problem with WinPcap 4.1.3 and Intel WIreless
Message-ID: <000001ced013$b9d7d990$2d878cb0$@myself.com>

I have used WinPcap for a long time and coming up to recent times, it works
just fine on Windows 7 Pro 64-bit ThinkPad with Intel 3945 G Wireless
Network Adapter.

 

I now have a ThinkPad X230 Windows 8 Pro 64-bit and an Intel 6205 N Wireless
Adapter. The Intel software comes with Intel ProSet which in turn manages
Wireless Hotspot which was new in Windows 8. 

 

If I install (and therefore run) WinPcap V4.1.3 on this Windows 8 machine,
the Intel ProSet Wireless Zero Configuration Service "stopped working" . I
get:

 

Faulting Application Path:             C:\Program
Files\Intel\WiFi\bin\ZeroConfigService.exe

Problem Event Name:   APPCRASH

Application Name:           ZeroConfigService.exe

Application Version:       16.5.0.0

 

I had tried this in July this year with the Lenovo version V15.05 xxx
Wireless Driver and got the same Stopped Working error, same crash details
as above but different version.

 

At the beginning of October, I switched to the Intel Version 16.5.3 Wireless
Driver. This appears to be a really good driver and it fixed some other
issues with Lenovo Software. So I thought I would try WinPcap again. But I
got the error above. 

 

So it appears there is a conflict between WinPcap and Intel 6205 ProSet and
Wireless Driver.

 

Thank you for any light you might shed on this.   . J Hurst

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.winpcap.org/pipermail/winpcap-users/attachments/20131023/115ffeee/attachment.html>

From Phil.Newlon at wendys.com  Thu Oct 24 17:53:45 2013
From: Phil.Newlon at wendys.com (Newlon, Phil)
Date: Thu, 24 Oct 2013 17:53:45 +0000
Subject: [Winpcap-users] capture differences between Linux system and
	Windows with winpcap
Message-ID: <A6F8DAAA3DA61740B9F118EB2B71232E01940914A7@WPEXCV08.Central.HQ.Internal>

I posted this question on wireshark site but the more I think about it (since results are the same with wireshark and windump) the more I think it is winpcap related.


Wireshark 1.10.2 (64 bit) on Windows 7, Wireshark 1.10 on Ubuntu 13.04 (compiled from source), WinPcap 4.1.3

I have been troubleshooting a network that contains several Windows Embedded Std 7 POS systems and a back office PC that runs Win 7 Pro. When I first looked at the network I was amazed at the volume of errors (dup ack, retrans, tcp out of order). My initial look was with the back office PC on a hub with a laptop running Win 7 Pro and wireshark. Wondering if I had an interface issue, I put a netoptics tap on the back office PC connection. My windows laptop only has one wired ethernet interface so I put a Ubuntu Linux box with two wired interfaces and when I captured with it the errors magically 'disappeared'. I then bought a USB -> wired ethernet dongle for my laptop and ran wireshark on both interfaces (still on the tap) and the errors show again. I have never seen this situation before and don't know where to turn next - I can't trust any captures done on my Windows 7 laptop now and can't take my Linux desktop PC with me on the road!

Why would Wireshark on windows be showing that the network has errors (thousands of them per minute) but on Linux it is clean?

If I run a tcpdump (or dumpcap) capture on the Linux box then copy the file to the Windows machine, it does not have the errors showing.

Maybe pertinent as well.... I used an Ubuntu Live CD in the laptop that normally runs Windows and captured via tcpdump (using built in interface and USB->ethernet dongle) and came up with a clean capture that way as well. I then used windump and captured two separate files (one from each interface) and merged them. That was even worse.

This is definitely a difference between windows and linux and how they capture but I can't fathom how there can be such a difference.

Any insight would be muchly appreciated!
Notice: This e-mail message and its attachments are the property of The Wendy's Company or one of its subsidiaries and may contain confidential or legally privileged information intended solely for the use of the addressee(s). If you are not an intended recipient, then any use, copying or distribution of this message or its attachments is strictly prohibited. If you received this message in error, please notify the sender and delete this message entirely from your system.

From Phil.Newlon at wendys.com  Fri Oct 25 15:36:16 2013
From: Phil.Newlon at wendys.com (Newlon, Phil)
Date: Fri, 25 Oct 2013 15:36:16 +0000
Subject: [Winpcap-users] capture differences between Linux system and
 Windows with winpcap
Message-ID: <A6F8DAAA3DA61740B9F118EB2B71232E0194093F61@WPEXCV08.Central.HQ.Internal>

I have found that this is machine specific (new Dell Latitude E6420).  I do not have the same issue with an older Dell D630....  Same results with built-in interface and with TrendNet USB->ethernet dongle.  Anyone have thoughts on what might be causing this or how to track down the cause?
Notice: This e-mail message and its attachments are the property of The Wendy's Company or one of its subsidiaries and may contain confidential or legally privileged information intended solely for the use of the addressee(s). If you are not an intended recipient, then any use, copying or distribution of this message or its attachments is strictly prohibited. If you received this message in error, please notify the sender and delete this message entirely from your system.

From winpcap-users-20040408 at subscriptions.pizzolato.net  Fri Oct 25 15:53:54 2013
From: winpcap-users-20040408 at subscriptions.pizzolato.net (Mark Pizzolato - WinPCap-Users)
Date: Fri, 25 Oct 2013 08:53:54 -0700
Subject: [Winpcap-users] capture differences between Linux system and
 Windows with winpcap
In-Reply-To: <A6F8DAAA3DA61740B9F118EB2B71232E0194093F61@WPEXCV08.Central.HQ.Internal>
References: <A6F8DAAA3DA61740B9F118EB2B71232E0194093F61@WPEXCV08.Central.HQ.Internal>
Message-ID: <0CC6789C1C831B4C8CCFF49D45D7010FDF90B5F01E@REDROOF2.alohasunset.com>

On Friday, October 25, 2013 at 8:36 AM, Newlon, Phil wrote:
> I have found that this is machine specific (new Dell Latitude E6420).  I do not
> have the same issue with an older Dell D630....  Same results with built-in
> interface and with TrendNet USB->ethernet dongle.  Anyone have thoughts
> on what might be causing this or how to track down the cause?

It sounds like the windows drivers for the wired Ethernet are somehow auto-detecting duplex or link speed incorrectly and this setting you up for the errors which you notice.

You may want to try various combinations of hard setup values to prove this issue right or wrong.

Good Luck,

- Mark Pizzolato


From Phil.Newlon at wendys.com  Fri Oct 25 19:32:24 2013
From: Phil.Newlon at wendys.com (Newlon, Phil)
Date: Fri, 25 Oct 2013 19:32:24 +0000
Subject: [Winpcap-users] capture differences between Linux system and
 Windows with winpcap
In-Reply-To: <0CC6789C1C831B4C8CCFF49D45D7010FDF90B5F01E@REDROOF2.alohasunset.com>
References: <A6F8DAAA3DA61740B9F118EB2B71232E0194093F61@WPEXCV08.Central.HQ.Internal>
 <0CC6789C1C831B4C8CCFF49D45D7010FDF90B5F01E@REDROOF2.alohasunset.com>
Message-ID: <A6F8DAAA3DA61740B9F118EB2B71232E0194096270@WPEXCV08.Central.HQ.Internal>

> It sounds like the windows drivers for the wired Ethernet are somehow auto-detecting duplex or link speed incorrectly and this setting you up for the errors which you notice.

I have tested forcing half duplex with a hub to sniff, and full duplex on a network tap.  Results are the same.
Notice: This e-mail message and its attachments are the property of The Wendy's Company or one of its subsidiaries and may contain confidential or legally privileged information intended solely for the use of the addressee(s). If you are not an intended recipient, then any use, copying or distribution of this message or its attachments is strictly prohibited. If you received this message in error, please notify the sender and delete this message entirely from your system.

From serge.wenger at matisa.ch  Thu Oct 31 09:01:08 2013
From: serge.wenger at matisa.ch (Wenger Serge)
Date: Thu, 31 Oct 2013 10:01:08 +0100
Subject: [Winpcap-users] Support of pcap-ng file
Message-ID: <18D98CA3D025CE42B26CFB3CF4FBD80603DC4FB5@matserv-exch.matisa.ch>

Hello,

 

I need to support Wireshark pcapng file. Did you have a roadmap for this
new feature?

 

Thanks

 

SW

 

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.winpcap.org/pipermail/winpcap-users/attachments/20131031/fbb9bd86/attachment.html>

From rogerbertrandsavoie at hotmail.com  Thu Oct 31 18:37:43 2013
From: rogerbertrandsavoie at hotmail.com (Roger Bertrand)
Date: Thu, 31 Oct 2013 13:37:43 -0500
Subject: [Winpcap-users] SUP - Winpcap-users Support of pcap-ng file
In-Reply-To: <18D98CA3D025CE42B26CFB3CF4FBD80603DC4FB5@matserv-exch.matisa.ch>
References: <18D98CA3D025CE42B26CFB3CF4FBD80603DC4FB5@matserv-exch.matisa.ch>
Message-ID: <BLU0-SMTP926062AAB044C54D9ED9DCBD0B0@phx.gbl>

Hello,

 

Sorry could you explain, I do not understand what you need.

 

Kind Regards,

 

Roger Bertrand, P.Eng.

 

Sr. Business Consultant

Microsoft Active Professional 2013 

 

Tel: 51-1-261-4397

Cel: 51-1-99-918-6000

USA: 1-518-772-1357

 

De: winpcap-users-bounces at winpcap.org
[mailto:winpcap-users-bounces at winpcap.org] En nombre de Wenger Serge
Enviado el: jueves, 31 de octubre de 2013 4:01
Para: winpcap-users at winpcap.org
Asunto: [Winpcap-users] Support of pcap-ng file

 

Hello,

 

I need to support Wireshark pcapng file. Did you have a roadmap for this new
feature?

 

Thanks

 

SW

 

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.winpcap.org/pipermail/winpcap-users/attachments/20131031/af6509c4/attachment.html>

