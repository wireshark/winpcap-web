#include "main.h"

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    switch (fdwReason)
    {
        case DLL_PROCESS_ATTACH:
            // attach to process
            break;

        case DLL_PROCESS_DETACH:
            // detach from process
            break;

        case DLL_THREAD_ATTACH:
            // attach to thread
            break;

        case DLL_THREAD_DETACH:
            // detach from thread
            break;
    }
    return TRUE; // succesful
}


/*=============================================================================
  Variables statiques.
=============================================================================*/
static  pcap_t      *adhandle = NULL;     /* Handle to access device */
static  HANDLE      hThread;

/** ------------------------------------------------------------------
 * \brief Ouverture d'un device Ethernet. Utilise un num�ro de device en entr�e.
 *        Le num�ro de device peut �tre r�cup�r� avec la fonction EdrEth_EnumDevice
 *
 * \param[in]   NumeroCarte Num�ro du device Ethernet � ouvrir.
 *
 * \return  \li TErreur :
 *          \li ER_FAILED_TO_ACESS_DEVICE
 *          \li ER_FAILED_TO_OBTAIN_MAC_ADDRESS
 *          \li ER_FAILED_TO_OPEN_DEVICE
 *          \li ER_SYSTEM_ERROR
 *                                                                    */
 /* ----------------------------------------------------------------- */
int DLL_EXPORT EdrEth_OpenDevice(int NumeroCarte)
{
    int                 Erreur = 0;
	pcap_if_t           *alldevs;
	pcap_if_t           *d;
	int                 i=0;
	char                errbuf[PCAP_ERRBUF_SIZE];
    DWORD               dwThreadId;

 	/* Retrieve the device list */
	if(pcap_findalldevs(&alldevs, errbuf) == -1)
	{
		fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
		/* Impossible d'acc�der au driver pcap */
		Erreur = -1;
		return Erreur;
	}
	/* Jump to the selected adapter */
	for (i=0;i<NumeroCarte;i++)
	{
	    if (alldevs->next != NULL)
        {
            alldevs = alldevs->next;
        }
        else
        {
            Erreur = -1;
            return Erreur;
        }
	}

	/* Open the adapter */
	d = alldevs;
    if ((adhandle = pcap_open(d->name,	    // name of the device
                              65536,		// portion of the packet to capture.
                                            // 65536 grants that the whole packet will be captured on all the MACs.
                              PCAP_OPENFLAG_PROMISCUOUS|PCAP_OPENFLAG_MAX_RESPONSIVENESS ,			// promiscuous mode (nonzero means promiscuous)
                                                                                                    // MAX_RESPONSIVENESS retourne d�s qu'un message est dans le buffer
                              1000,	        // read timeout (1000 ms)
                              NULL,         // authentification
                              errbuf		// error buffer
                              )) == NULL)

    {
        fprintf(stderr,"\nUnable to open the adapter. %s is not supported by WinPcap\n",d->name);

        /* Free the device list */
        pcap_freealldevs(alldevs);

        /* Impossible d'acc�der au driver pcap */
		Erreur = -1;
        return Erreur;
    }
    /* Free the device list */
    pcap_freealldevs(alldevs);

    /* Create thread to capture the live packet */
    hThread = CreateThread( NULL,                   // default security attributes
                            0,                      // use default stack size
                            pcapLoopThread,         // thread function name
                            NULL,                   // argument to thread function
                            0,                      // use default creation flags
                            &dwThreadId);           // returns the thread identifier

    if (hThread == NULL)
    {
		/* Impossible de creer un thread */
        fprintf(stderr,"Error in CreateThread\n");
        pcap_close(adhandle);           /* Fermeture de WinPcap */
		Erreur = -1;
    }
    return(Erreur);
}

/** ------------------------------------------------------------------
 * \brief Thread de lecture des paquets Ethernet. Boucle infinie
 *
 * \param[in]   lpParam Not used.
 *
 * \return  0
 *                                                                    */
 /* ----------------------------------------------------------------- */
DWORD WINAPI pcapLoopThread( LPVOID lpParam )
{
    /* start the capture */
    pcap_loop(adhandle, 0, packet_handler, NULL);
    return 0;
}

/** ------------------------------------------------------------------
 * \brief Callback function invoked by libpcap for every incoming packet.
 *        Filtre les paquets re�u par l'adresse MAC EDR
 *
 * \param[in]   param Not used
 * \param[in]   header Header du paquet Ethernet re�u
 * \param[in]   pkt_data Donn�es du paquet Ethernet
 *
 * \return aucun
 *                                                                    */
 /* ----------------------------------------------------------------- */
void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data)
{
    // Do nothing
}

/** ------------------------------------------------------------------
 * \brief Ferme le device Ethernet et d�truit la tache de r�ception de paquet
 *
 * \return TErreur
 *                                                                    */
 /* ----------------------------------------------------------------- */
int DLL_EXPORT EdrEth_CloseDevice()
{
    int Erreur = 0;

    /* Test if device has been opened */
    if (adhandle == NULL)
    {
        Erreur = -1;
        return Erreur;
    }
    else
    {
        /* Close it */
        pcap_close(adhandle);
        adhandle = NULL;
    }
    return Erreur;
}
