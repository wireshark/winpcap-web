#ifndef __MAIN_H__
#define __MAIN_H__

#include <windows.h>

/*  To use this exported function of dll, include this header
 *  in your project.
 */


#ifdef __cplusplus
extern "C"
{
#endif


#ifdef BUILD_DLL
    #define DLL_EXPORT __declspec(dllexport)
#else
    #define DLL_EXPORT __declspec(dllimport)
#endif

#define HAVE_REMOTE
#include "pcap.h"       /* Utilisation de la librairie WinPCAP */


void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data);
DWORD WINAPI pcapLoopThread( LPVOID lpParam );
int DLL_EXPORT EdrEth_OpenDevice(int NumeroCarte);
int DLL_EXPORT EdrEth_CloseDevice();

#ifdef __cplusplus
}
#endif

#endif // __MAIN_H__
