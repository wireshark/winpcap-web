From gianluca.varenni at cacetech.com  Fri Nov  9 17:47:00 2007
From: gianluca.varenni at cacetech.com (Gianluca Varenni)
Date: Fri Nov  9 18:02:29 2007
Subject: [Winpcap-announce] WinPcap 4.0.2 has been released
Message-ID: <0e2f01c822f8$8805bb90$1a4da8c0@NELSON2>

As of today, WinPcap 4.0.2 is available in the download section of the
WinPcap website, http://www.winpcap.org/install/ .
 
This maintenance release addresses a security vulnerability reported 
by the iDefense Labs in a soon-to-be-released advisory. 
Full details can be found in the change log attached at the end of 
this message. 

Gianluca Varenni
WinPcap Team



Changelog from WinPcap 4.0.1
============================

- Disabled support for monitor mode (also called TME, Table Management
  Extensions) in the driver. This module suffers from several security
  vulnerabilities that could result in BSODs or privilege escalation 
  attacks. This fix addresses a security vulnerability reported by the
  iDefense Labs. 
  
- Bug fixing:
  * Added a missing NULL pointer check in pcap_open() 
  * Fixed a misplaced #ifdef WIN32 directive in pcap_open(). 
  * Fixed a bug in the send routine of the driver that could cause a 
    crash under low resources conditions. 
  * Fixed a bug in the installer causing a mis-detection of a previous
    WinPcap installation 
  * Minor cleanup of some #define directives in the driver (to disable
    the TME extensions).

=========




  
-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/x-pkcs7-signature
Size: 2815 bytes
Desc: not available
Url : http://www.winpcap.org/pipermail/winpcap-announce/attachments/20071109/fc2d9d68/smime.bin
From gianluca.varenni at cacetech.com  Thu Nov 15 17:22:08 2007
From: gianluca.varenni at cacetech.com (Gianluca Varenni)
Date: Thu Nov 15 17:31:06 2007
Subject: [Winpcap-announce] WinPcap 4.1 beta2 has been released
Message-ID: <00c501c827ac$0cfc5220$1a4da8c0@NELSON2>

As of today, WinPcap 4.1 beta2 is available in the download section of
the WinPcap website, http://www.winpcap.org/install/ .

This new software release includes several improvements and changes to
both the library itself and its developer's pack. First of all, it 
fixes a security vulnerability in the kernel driver reported by the 
iDefense Labs in the security advisory available at
  http://labs.idefense.com/intelligence/vulnerabilities/display.php?id=625

It also includes the latest available snapshot of libpcap (1.0 
branch).
>From the developer's point of view, this version ships with a cleaned 
up update of the developer's pack. Some header files that were wrongly
included in the old developer's pack (including some coming from the 
Microsoft platform SDK) have been removed. Other files have been 
consolidated or split into internal header files (used for the build 
of the binaries) and public header files.
Full details can be found in the change log attached at the end of 
this message. 

Being a beta release, as usual, we encourage people to test it and 
report any anomaly or strange behavior to the WinPcap mailing lists. 

In particular, we strongly encourage all the developers to try 
compiling all their WinPcap-based applications against the new WinPcap
developer's pack and report any compilation issue to the winpcap-bugs 
mailing list (winpcap-bugs<AT>winpcap.org).

Gianluca Varenni
WinPcap Team



Changelog from WinPcap 4.0.1
============================

- Disabled support for monitor mode (also called TME, Table Management
  Extensions) in the driver. This module suffers from several security
  vulnerabilities that could result in BSODs or privilege escalation 
  attacks. This fix addresses a security vulnerability reported by the
  iDefense Labs at 
  http://labs.idefense.com/intelligence/vulnerabilities/display.php?id=625

- Added a small script to integrate the libpcap sources into the 
  WinPcap tree automatically.

- Moved the definition of all the I/O control codes to ioctls.h.

- Cleaned up and removed some build scripts for the developer's pack.

- Migrated the driver compilation environment to WDK 6000.

- Enabled PreFAST driver compilation for the x64 build.

- Added some doxygen directives to group the IOCTL codes and JIT 
  definitions in proper groups.

- Integrated the IOCTL codes into one single set shared by packet.dll 
  and driver.

- Modified the installer to return the win32 error code instead of -1 
  in case of failure in the error messages.

- Added some #define directives to selectively disable the TME 
  functionality for WAN (i.e. Netmon-assisted) devices.

- Added a VS2005 project to easily edit the files of the driver.

- Removed some useless #include directives in the driver and 
  packet.dll.

- Migrated several conditional directives (#ifdef/#endif) to the 
  defines of the DDK/WDK e.g. _X86_ and _AMD64_.

- Added a check to warn users that remote-ext.h should not be included
  directly.

- Removed ntddndis.h from the WinPcap sources. It's included into the
  Microsoft Platform SDK.

- Removed devioctl.h from the WinPcap sources. It's included into the 
  Microsoft DDK/WDK.
  
- Removed ntddpack.h from the WinPcap sources. It's an old header file
  from the original DDK Packet sample, and it's not used by WinPcap.

- Removed several useless files from the WinPcap developer's pack:
  + all the TME extension header files
  + devioctl.h
  + gnuc.h
  + ntddndis.h
  + ntddpack.h
  + pcap-int.h.

- Bug fixing:
  + Fixed a possible buffer overrun on x64 machines with more that 32 
    CPUs/cores.
  + Fixed an implicit cast problem compiling the driver on x64.
  + Fixed a bug in the installer causing a mis-detection of a previous
    WinPcap installation.
  + Fixed two bugs related to memory deallocation in packet.dll. We 
    were using free() instead of GlobalFreePtr(), and there was a 
    missing check as to when to deallocate a chunk of memory.
  + Added a missing NULL pointer check in pcap_open().
  + Moved a misplaced #ifdef WIN32 in pcap_open().
  + Fixed a bug in the send routine of the driver that could cause a 
    crash under low resources conditions.

=========
-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/x-pkcs7-signature
Size: 2815 bytes
Desc: not available
Url : http://www.winpcap.org/pipermail/winpcap-announce/attachments/20071115/3817d572/smime.bin
