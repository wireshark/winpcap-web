From gianluca.varenni at cacetech.com  Tue Oct 20 09:25:47 2009
From: gianluca.varenni at cacetech.com (Gianluca Varenni)
Date: Tue, 20 Oct 2009 09:25:47 -0700
Subject: [Winpcap-announce] WinPcap 4.1 has been released
Message-ID: <0435E77E35984D198911E9EA00B5D0B8@NELSON3>

As of today, WinPcap 4.1 is available in the download section of the
WinPcap website.

This release contains a large series of improvements that were gradually
added to WinPcap during the various beta's.

First of all, this version includes full support for x64 platforms, both
in the driver and in the user level libraries.
Also, the long awaited support for Windows 7 (and Windows Server 2008 R2)
has been added to the long list of supported flavors of Windows.
The installer has been greatly improved and partially rewritten to better
handle error conditions and non-standard Windows configurations.
Finally, it included the latest version of libpcap (from
http://www.tcpdump.org/) in the 1.0 branch.
Full details of the changes can be found in the change log below.

As always, we profoundly thank all the users that tested the development
versions of WinPcap 4.1. Thanks!

Gianluca Varenni
WinPcap Team



Changelog from WinPcap 4.1 beta5
================================

- Several fixes and updates to the installer:
  + Added installation support for Windows 7 and Server 2008 R2
  + Added a new wizard page to choose if the driver should be started
    automatically on boot.
  + Fixed some issues when upgrading WinPcap on Windows Vista and Server
    2008 x64.
  + Better handle errors when Microsoft NetMon 2.x is not available.
  + Better detection of the target operating system, especially when the
    installer is run in compatibility mode.

- wpcap.dll has been updated to the 1.0 branch of libpcap from
  http://www.tcpdump.org.

- Updated the tools used for the compilation (WDK 6001.18002).

- Bug fixing:
  + Exported pcap_setdirection()
  + Fixed a bug in the compilation of rpcapd. This bug was causing the
    daemon not to capture any packets.


Changelog between WinPcap 4.1 beta4 and  WinPcap 4.1 beta5
==========================================================

- Starting from this build, WinPcap is completely compiled with Visual
  Studio 2005 SP1 (in order to have a single build environment for x86
  and x64) and WDK6000. While the projects for Visual Studio 6 are still
  available in the source package, they are no longer maintained.

- wpcap.dll has been updated to libpcap 1.0.0 from http://www.tcpdump.org.

- The new VS2005 project files for wpcap.dll and packet.dll have been
  simplified a lot (i.e. less configurations!).

- Big parts of the installer have been rewritten and cleaned up to account
  for the x64 binaries installation.

- The old WanPacket DLL has been removed. The code has been merged into
  packet.dll.

- The developer's pack includes LIB files for both x86 and x64 (for Visual
  Studio). At the moment we don't have the LIB files for Cygwin under x64.

- The samples have been ported to Visual Studio 2005, and they compile for
  both x86 and x64 architectures. The old Visual Studio 6 projects are
  still available but not actively maintained.

- Bug fixing:
  + Fixed the remote code to make it compile properly on Linux.
  + Fixed a problem with the icon in the windows control panel.
  + Fixed an installation bug under x64 for rpcapd.exe. When installing
    rpcapd on an x64 machine, the executable is located in c:\program files
    (x86), not in c:\program files.
  + Support an indefinite number of IP (v4 and v6) addresses associated with
    an adapter.
  + Check that IPv4 is bound to an adapter before getting the IPv4 addresses
    from the registry.
  + Fixed several compilation warnings in the samples.
  + Exported pcap_hopen_offline.
  + Added a missing definition of HAVE_UINT64 in the bittypes.h.
  + Fixed a bug in the filtering code for TurboCap adapters. The snaplen was
    completely ignored.


Changelog between WinPcap 4.1 beta3 and  WinPcap 4.1 beta4
==========================================================

- Added support for the CACE TurboCap boards within wpcap.dll.

- (from libpcap) Added the new functions pcap_create(),
  pcap_activate(), pcap_set_XXX() (still not completely documented on
  Windows).

- (from libpcap) Added support for various MAC addresses' syntaxes.
  Now the following syntaxes are supported:
  + 00:11:22:33:44:55
  + 00-11-22-33-44-55
  + 00.11.22.33.44.55
  + 001122334455.

- Bug fixing:
  + Use FILE_DEVICE_SECURE_OPEN as a parameter to IoCreateDevice()
    when creating the I/O device from within the driver on the OSes
    that support it.
  + Fixed a bug in pcap_open_live() and pcap_activate(). They were
    failing if called on a local adapter with the syntax
    rpcap://\\Device....
  + Added a missing input buffer check in the read handler of the
    driver when working in statistics mode.
  + Optimized the code in the driver that handles the BIOCGSTATS
    control code (map only the needed portion of the user buffer into
    an MDL).
  + Fixed a possible memory leak in one of the error paths of the
    driver when enumerating the available adapters.
  + Cleaned up some global variable names in the driver.


Changelog between WinPcap 4.1 beta2 and  WinPcap 4.1 beta3
==========================================================

- (from libpcap) Make some arguments of some pcap functions const
  pointers if that makes sense.

- (from libpcap) Add some additional checks to bpf_validate(), from
  OpenBSD.

- (from libpcap) Use bpf_validate() in install_bpf_program(), so we
  validate programs even when they're being processed by userland
  filters.

- (from libpcap) Get rid of BPF_MAXINSNS - we don't have a limit on
  program size in libpcap/WinPcap.

- (from libpcap) Support for the "addr1", "addr2", "addr3", and
  "addr4" link-layer address filtering keywords for 802.11.

- (from libpcap) Support for filtering over 802.11 frame types with
  the keywords "type" and "subtype".

- Bug fixing:
  + Fixed a bug when generating wireless filters in the form "link src
    host ...". The source address was not retrieved properly.
  + Added some more logic in the installer to account for errors while
    installing the Network Monitor component (NetMon). If NetMon is
    not available, we install a version of packet.dll that doesn't
    depend on it.
  + Fixed two bugs in the original OpenBSD filter validation code, one
    that caused it to reject all filters that used multiply
    instructions, and another that caused it to reject all filters
    that used divide instructions.
  + Fixed a bug in the filter engine in the driver. When the packet to
    filter is split into two buffers, under some circumstances the
    engine was not checking the right bytes in the packet.


Changelog between WinPcap 4.1 beta and  WinPcap 4.1 beta2
=========================================================

- Disabled support for monitor mode (also called TME, Table Management
  Extensions) in the driver. This module suffers from several security
  vulnerabilities that could result in BSODs or privilege escalation
  attacks. This fix addresses a security vulnerability reported by the
  iDefense Labs at
  http://labs.idefense.com/intelligence/vulnerabilities/display.php?id=625

- Added a small script to integrate the libpcap sources into the
  WinPcap tree automatically.

- Moved the definition of all the I/O control codes to ioctls.h.

- Cleaned up and removed some build scripts for the developer's pack.

- Migrated the driver compilation environment to WDK 6000.

- Enabled PreFAST driver compilation for the x64 build.

- Added some doxygen directives to group the IOCTL codes and JIT
  definitions in proper groups.

- Integrated the IOCTL codes into one single set shared by packet.dll
  and driver.

- Modified the installer to return the win32 error code instead of -1
  in case of failure in the error messages.

- Added some #define directives to selectively disable the TME
  functionality for WAN (i.e. Netmon-assisted) devices.

- Added a VS2005 project to easily edit the files of the driver.

- Removed some useless #include directives in the driver and
  packet.dll.

- Migrated several conditional directives (#ifdef/#endif) to the
  defines of the DDK/WDK e.g. _X86_ and _AMD64_.

- Added a check to warn users that remote-ext.h should not be included
  directly.

- Removed ntddndis.h from the WinPcap sources. It's included into the
  Microsoft Platform SDK.

- Removed devioctl.h from the WinPcap sources. It's included into the
  Microsoft DDK/WDK.

- Removed ntddpack.h from the WinPcap sources. It's an old header file
  from the original DDK Packet sample, and it's not used by WinPcap.

- Removed several useless files from the WinPcap developer's pack:
  + all the TME extension header files
  + devioctl.h
  + gnuc.h
  + ntddndis.h
  + ntddpack.h
  + pcap-int.h.

- Bug fixing:
  + Fixed a possible buffer overrun on x64 machines with more that 32
    CPUs/cores.
  + Fixed an implicit cast problem compiling the driver on x64.
  + Fixed a bug in the installer causing a mis-detection of a previous
    WinPcap installation.
  + Fixed two bugs related to memory deallocation in packet.dll. We
    were using free() instead of GlobalFreePtr(), and there was a
    missing check as to when to deallocate a chunk of memory.
  + Added a missing NULL pointer check in pcap_open().
  + Moved a misplaced #ifdef WIN32 in pcap_open().
  + Fixed a bug in the send routine of the driver that could cause a
    crash under low resources conditions.


Changelog between WinPcap 4.0.1 and  WinPcap 4.1 beta
=====================================================

- Added support for the Per Packet Info (PPI) link type.

- wpcap.dll has been updated to the libpcap 0.9.6 branch from
  http://www.tcpdump.org.

- Bug fixing:
  + Fixed a bug in pcap_open_live() by which we were silently ignoring
    a failure when switching into promiscuous mode. This fix solves
    the outstanding issue of wireless cards that fail to go into
    promiscuous mode and do not capture any packet.
  + Experimental fixes to the BPF compiler (pcap_compile()) to better
    support filters over 802.11.
  + Minor fixes to remove several PFD (PreFAST for Drivers) warnings.
  + (from libpcap 0.9.6) added additional filter operations for 802.11
    frame types
  + (from libpcap 0.9.6) fixes to discard unread packets when changing
    filters.



From gianluca.varenni at cacetech.com  Tue Oct 20 17:27:09 2009
From: gianluca.varenni at cacetech.com (Gianluca Varenni)
Date: Tue, 20 Oct 2009 17:27:09 -0700
Subject: [Winpcap-announce] WinPcap 4.1.1 is available
Message-ID: <59E1B42A8B714A13BE4A40729F74D0F3@NELSON3>

As of now, WinPcap 4.1.1 is available in the download section of the
WinPcap website, http://www.winpcap.org/

This release fixes a bug in the installer of WinPcap 4.1, released earlier
today. The binaries were not digitally signed, thus preventing the WinPcap
driver from working on Vista/2008/Win7/2008R2 x64.

Full details of the changes can be found in the change log below.

Gianluca Varenni
WinPcap Team

Changelog from WinPcap 4.1
==========================

- This release fixes a bug in the installer of WinPcap 4.1. The binaries
  were not digitally signed, thus preventing the WinPcap driver from
  working on Vista/2008/Win7/2008R2 x64.


Changelog from WinPcap 4.1 beta5
================================

- Several fixes and updates to the installer:
  + Added installation support for Windows 7 and Server 2008 R2
  + Added a new wizard page to choose if the driver should be started
    automatically on boot.
  + Fixed some issues when upgrading WinPcap on Windows Vista and Server
    2008 x64.
  + Better handle errors when Microsoft NetMon 2.x is not available.
  + Better detection of the target operating system, especially when the
    installer is run in compatibility mode.

- wpcap.dll has been updated to the 1.0 branch of libpcap from
  http://www.tcpdump.org.

- Updated the tools used for the compilation (WDK 6001.18002).

- Bug fixing:
  + Exported pcap_setdirection()
  + Fixed a bug in the compilation of rpcapd. This bug was causing the
    daemon not to capture any packets.


Changelog between WinPcap 4.1 beta4 and  WinPcap 4.1 beta5
==========================================================

- Starting from this build, WinPcap is completely compiled with Visual
  Studio 2005 SP1 (in order to have a single build environment for x86
  and x64) and WDK6000. While the projects for Visual Studio 6 are still
  available in the source package, they are no longer maintained.

- wpcap.dll has been updated to libpcap 1.0.0 from http://www.tcpdump.org.

- The new VS2005 project files for wpcap.dll and packet.dll have been
  simplified a lot (i.e. less configurations!).

- Big parts of the installer have been rewritten and cleaned up to account
  for the x64 binaries installation.

- The old WanPacket DLL has been removed. The code has been merged into
  packet.dll.

- The developer's pack includes LIB files for both x86 and x64 (for Visual
  Studio). At the moment we don't have the LIB files for Cygwin under x64.

- The samples have been ported to Visual Studio 2005, and they compile for
  both x86 and x64 architectures. The old Visual Studio 6 projects are
  still available but not actively maintained.

- Bug fixing:
  + Fixed the remote code to make it compile properly on Linux.
  + Fixed a problem with the icon in the windows control panel.
  + Fixed an installation bug under x64 for rpcapd.exe. When installing
    rpcapd on an x64 machine, the executable is located in c:\program files
    (x86), not in c:\program files.
  + Support an indefinite number of IP (v4 and v6) addresses associated with
    an adapter.
  + Check that IPv4 is bound to an adapter before getting the IPv4 addresses
    from the registry.
  + Fixed several compilation warnings in the samples.
  + Exported pcap_hopen_offline.
  + Added a missing definition of HAVE_UINT64 in the bittypes.h.
  + Fixed a bug in the filtering code for TurboCap adapters. The snaplen was
    completely ignored.


Changelog between WinPcap 4.1 beta3 and  WinPcap 4.1 beta4
==========================================================

- Added support for the CACE TurboCap boards within wpcap.dll.

- (from libpcap) Added the new functions pcap_create(),
  pcap_activate(), pcap_set_XXX() (still not completely documented on
  Windows).

- (from libpcap) Added support for various MAC addresses' syntaxes.
  Now the following syntaxes are supported:
  + 00:11:22:33:44:55
  + 00-11-22-33-44-55
  + 00.11.22.33.44.55
  + 001122334455.

- Bug fixing:
  + Use FILE_DEVICE_SECURE_OPEN as a parameter to IoCreateDevice()
    when creating the I/O device from within the driver on the OSes
    that support it.
  + Fixed a bug in pcap_open_live() and pcap_activate(). They were
    failing if called on a local adapter with the syntax
    rpcap://\\Device....
  + Added a missing input buffer check in the read handler of the
    driver when working in statistics mode.
  + Optimized the code in the driver that handles the BIOCGSTATS
    control code (map only the needed portion of the user buffer into
    an MDL).
  + Fixed a possible memory leak in one of the error paths of the
    driver when enumerating the available adapters.
  + Cleaned up some global variable names in the driver.


Changelog between WinPcap 4.1 beta2 and  WinPcap 4.1 beta3
==========================================================

- (from libpcap) Make some arguments of some pcap functions const
  pointers if that makes sense.

- (from libpcap) Add some additional checks to bpf_validate(), from
  OpenBSD.

- (from libpcap) Use bpf_validate() in install_bpf_program(), so we
  validate programs even when they're being processed by userland
  filters.

- (from libpcap) Get rid of BPF_MAXINSNS - we don't have a limit on
  program size in libpcap/WinPcap.

- (from libpcap) Support for the "addr1", "addr2", "addr3", and
  "addr4" link-layer address filtering keywords for 802.11.

- (from libpcap) Support for filtering over 802.11 frame types with
  the keywords "type" and "subtype".

- Bug fixing:
  + Fixed a bug when generating wireless filters in the form "link src
    host ...". The source address was not retrieved properly.
  + Added some more logic in the installer to account for errors while
    installing the Network Monitor component (NetMon). If NetMon is
    not available, we install a version of packet.dll that doesn't
    depend on it.
  + Fixed two bugs in the original OpenBSD filter validation code, one
    that caused it to reject all filters that used multiply
    instructions, and another that caused it to reject all filters
    that used divide instructions.
  + Fixed a bug in the filter engine in the driver. When the packet to
    filter is split into two buffers, under some circumstances the
    engine was not checking the right bytes in the packet.


Changelog between WinPcap 4.1 beta and  WinPcap 4.1 beta2
=========================================================

- Disabled support for monitor mode (also called TME, Table Management
  Extensions) in the driver. This module suffers from several security
  vulnerabilities that could result in BSODs or privilege escalation
  attacks. This fix addresses a security vulnerability reported by the
  iDefense Labs at
  http://labs.idefense.com/intelligence/vulnerabilities/display.php?id=625

- Added a small script to integrate the libpcap sources into the
  WinPcap tree automatically.

- Moved the definition of all the I/O control codes to ioctls.h.

- Cleaned up and removed some build scripts for the developer's pack.

- Migrated the driver compilation environment to WDK 6000.

- Enabled PreFAST driver compilation for the x64 build.

- Added some doxygen directives to group the IOCTL codes and JIT
  definitions in proper groups.

- Integrated the IOCTL codes into one single set shared by packet.dll
  and driver.

- Modified the installer to return the win32 error code instead of -1
  in case of failure in the error messages.

- Added some #define directives to selectively disable the TME
  functionality for WAN (i.e. Netmon-assisted) devices.

- Added a VS2005 project to easily edit the files of the driver.

- Removed some useless #include directives in the driver and
  packet.dll.

- Migrated several conditional directives (#ifdef/#endif) to the
  defines of the DDK/WDK e.g. _X86_ and _AMD64_.

- Added a check to warn users that remote-ext.h should not be included
  directly.

- Removed ntddndis.h from the WinPcap sources. It's included into the
  Microsoft Platform SDK.

- Removed devioctl.h from the WinPcap sources. It's included into the
  Microsoft DDK/WDK.

- Removed ntddpack.h from the WinPcap sources. It's an old header file
  from the original DDK Packet sample, and it's not used by WinPcap.

- Removed several useless files from the WinPcap developer's pack:
  + all the TME extension header files
  + devioctl.h
  + gnuc.h
  + ntddndis.h
  + ntddpack.h
  + pcap-int.h.

- Bug fixing:
  + Fixed a possible buffer overrun on x64 machines with more that 32
    CPUs/cores.
  + Fixed an implicit cast problem compiling the driver on x64.
  + Fixed a bug in the installer causing a mis-detection of a previous
    WinPcap installation.
  + Fixed two bugs related to memory deallocation in packet.dll. We
    were using free() instead of GlobalFreePtr(), and there was a
    missing check as to when to deallocate a chunk of memory.
  + Added a missing NULL pointer check in pcap_open().
  + Moved a misplaced #ifdef WIN32 in pcap_open().
  + Fixed a bug in the send routine of the driver that could cause a
    crash under low resources conditions.


Changelog between WinPcap 4.0.1 and  WinPcap 4.1 beta
=====================================================

- Added support for the Per Packet Info (PPI) link type.

- wpcap.dll has been updated to the libpcap 0.9.6 branch from
  http://www.tcpdump.org.

- Bug fixing:
  + Fixed a bug in pcap_open_live() by which we were silently ignoring
    a failure when switching into promiscuous mode. This fix solves
    the outstanding issue of wireless cards that fail to go into
    promiscuous mode and do not capture any packet.
  + Experimental fixes to the BPF compiler (pcap_compile()) to better
    support filters over 802.11.
  + Minor fixes to remove several PFD (PreFAST for Drivers) warnings.
  + (from libpcap 0.9.6) added additional filter operations for 802.11
    frame types
  + (from libpcap 0.9.6) fixes to discard unread packets when changing
    filters.


