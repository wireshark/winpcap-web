From James.Mead at bkalogistics.net  Fri Jan 13 06:47:51 2012
From: James.Mead at bkalogistics.net (Jim Mead)
Date: Fri, 13 Jan 2012 09:47:51 -0500
Subject: [Windump] Out of Office AutoReply: Delivery reports about your
	e-mail
Message-ID: <105DD23F831FCC49BCDA5D1B8670C7737C1B7F@BKASRVR.bkalogisticsllc.local>

As of Dec 2009, Jim Mead is no longer employed by BKA Logistics.  Please direct all communications to Mark Millard.
Office Tel: 202-331-7395  
mark.millard at bkalogistics.net

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.winpcap.org/pipermail/windump/attachments/20120113/bd4b1144/attachment.html>

