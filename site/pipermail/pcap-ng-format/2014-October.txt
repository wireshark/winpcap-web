From guy at alum.mit.edu  Wed Oct 15 21:29:07 2014
From: guy at alum.mit.edu (Guy Harris)
Date: Wed, 15 Oct 2014 14:29:07 -0700
Subject: [pcap-ng-format] Should we register the domain pcap-ng.org and set
	up a Web site?
Message-ID: <A8EE459E-C644-4601-9001-31806BCA1D86@alum.mit.edu>

The Web site could host:

	the pcap-ng specification;

	the LINKTYPE_ list currently at http://www.tcpdump.org/linktypes.html;

	a pcap specification, if we decide to make one, similar to the pcap-ng specification.

(pcap.org was taken by the Prestressed Concrete Association of Pennsylvania, which changed their name to the Central Atlantic Bridge Associates but still has a page at www.pcap.org.)

From erik.hjelmvik at gmail.com  Thu Oct 16 06:25:35 2014
From: erik.hjelmvik at gmail.com (Erik Hjelmvik)
Date: Thu, 16 Oct 2014 08:25:35 +0200
Subject: [pcap-ng-format] Should we register the domain pcap-ng.org and
 set up a Web site?
In-Reply-To: <A8EE459E-C644-4601-9001-31806BCA1D86@alum.mit.edu>
References: <A8EE459E-C644-4601-9001-31806BCA1D86@alum.mit.edu>
Message-ID: <CAJEFHchKRYb+V=0kE7s8y4OYV2Go+jY3tgSAR7VLV+pKJ9Sbfg@mail.gmail.com>

Very good idea Guy,

However, it might be better to drop the hyphen in the domain name to
avoid odd characters and get a shorter and easier-to-type name. The
domain pcapng[.]org is still available.

/erik

2014-10-15 23:29 GMT+02:00 Guy Harris <guy at alum.mit.edu>:
> The Web site could host:
>
>         the pcap-ng specification;
>
>         the LINKTYPE_ list currently at http://www.tcpdump.org/linktypes.html;
>
>         a pcap specification, if we decide to make one, similar to the pcap-ng specification.
>
> (pcap.org was taken by the Prestressed Concrete Association of Pennsylvania, which changed their name to the Central Atlantic Bridge Associates but still has a page at www.pcap.org.)
> _______________________________________________
> pcap-ng-format mailing list
> pcap-ng-format at winpcap.org
> https://www.winpcap.org/mailman/listinfo/pcap-ng-format

From mcr at sandelman.ca  Thu Oct 16 13:15:22 2014
From: mcr at sandelman.ca (Michael Richardson)
Date: Thu, 16 Oct 2014 09:15:22 -0400
Subject: [pcap-ng-format] Should we register the domain pcap-ng.org and
	set up a Web site?
In-Reply-To: <CAJEFHchKRYb+V=0kE7s8y4OYV2Go+jY3tgSAR7VLV+pKJ9Sbfg@mail.gmail.com>
References: <A8EE459E-C644-4601-9001-31806BCA1D86@alum.mit.edu>
 <CAJEFHchKRYb+V=0kE7s8y4OYV2Go+jY3tgSAR7VLV+pKJ9Sbfg@mail.gmail.com>
Message-ID: <21269.1413465322@sandelman.ca>


Erik Hjelmvik <erik.hjelmvik at gmail.com> wrote:
    > However, it might be better to drop the hyphen in the domain name to
    > avoid odd characters and get a shorter and easier-to-type name. The
    > domain pcapng[.]org is still available.

good call.  We need both anyway.


