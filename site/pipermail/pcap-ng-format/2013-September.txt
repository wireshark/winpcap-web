From guy at alum.mit.edu  Sat Sep 28 20:33:15 2013
From: guy at alum.mit.edu (Guy Harris)
Date: Sat, 28 Sep 2013 13:33:15 -0700
Subject: [pcap-ng-format] The captured length of an SBP packet must take
	into account the packet length
Message-ID: <EF44E0AB-C3D1-4ECF-9BE5-C52D3E6BBAF2@alum.mit.edu>

To quote a comment I just checked into the pcap-ng support code in Wireshark, the captured length of a packet in an SPB must be the minimum of:

	the number of bytes available for packet data in the block (it obviously can't be greater than that);

	the snapshot length from the IDB (which should limit the length of all packets);

	the packet length (you can't capture bytes that aren't there).

The first of the three values will always be a multiple of 4 in a valid pcap-ng file, so there needs to be *some* way to eliminate padding.  The second value will do so if the snapshot length is less than the amount of bytes available for packet data, but not if it's greater; the third value will do so if the packet length is less than the amount of bytes available for packet data, but not if it's greater.  Therefore, we need to do both checks.

The spec speaks of the snapshot length, but not the packet length.  If we're not going to get rid of the SPB, we should update the spec to mention this.

From guy at alum.mit.edu  Sat Sep 28 20:49:25 2013
From: guy at alum.mit.edu (Guy Harris)
Date: Sat, 28 Sep 2013 13:49:25 -0700
Subject: [pcap-ng-format] The captured length of an SBP packet must take
	into account the packet length
In-Reply-To: <EF44E0AB-C3D1-4ECF-9BE5-C52D3E6BBAF2@alum.mit.edu>
References: <EF44E0AB-C3D1-4ECF-9BE5-C52D3E6BBAF2@alum.mit.edu>
Message-ID: <541B005E-4435-4C80-861F-1F836F582226@alum.mit.edu>


On Sep 28, 2013, at 1:33 PM, Guy Harris <guy at alum.mit.edu> wrote:

> The spec speaks of the snapshot length, but not the packet length.

No, it does:

	Packet Data: the data coming from the network, including link-layers headers. The length of this field can be derived from the field Block Total Length, present in the Block Header, and it is the minimum value among the SnapLen (present in the Interface Description Block) and the Packet Len (present in this header).

