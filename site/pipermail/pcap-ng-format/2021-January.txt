From mcr+ietf at sandelman.ca  Wed Jan  6 23:51:00 2021
From: mcr+ietf at sandelman.ca (Michael Richardson)
Date: Wed, 06 Jan 2021 18:51:00 -0500
Subject: [pcap-ng-format] about the large initial registry table for
	gharris-opsawg-pcap
In-Reply-To: <D3A753C3-ACCA-40B0-8028-E003DB29BF8A@sonic.net>
References: <21576.1609796505@localhost>
 <D3A753C3-ACCA-40B0-8028-E003DB29BF8A@sonic.net>
Message-ID: <5632.1609977060@localhost>


I asked IANA in ticket #1186419 two questions:

> 1) We wonder if IANA would prefer to receive this list in another
> form.
>    Your XML format as an appendix CODE BEGINs?
>    We likely would write code to turn the XML format into a table
>    for libpcap and wireshark...

IANA> This would be just fine, if it would make sense for you. This is our format:

<record date="yyyy-mm-dd">
<value>0</value>
<name>LINKTYPE_NULL</name>
<description>BSD loopback encapsulation</description>
<xref type="draft" data="draft-gharris-opsawg-pcap-01"/>
</record>
[plus more details]

mcr> and I think that it would make sense for us to provide it this way in an
mcr> CODE BEGINs appendix.  We have in a few machine readable formats, I
mcr> think that we (the-tcpdump-group, libpcap) can just make this our
mcr> canonical format. So we'd wget'ed from IANA after document publication.

---

I then asked:

> 2) We had previously reserved 147 thru 162 (LINKTYPE_RESERVED_xx) for
>    Private Use, but we are also specifying 65000 -> 65535 as such.

which would be clearer of several combinations:

>    Would it be clearer to write:
>
> *  values from 0 to 32767 are marked as Specification Required.
>
> *  values from 32768 to 65000 are marked as First-Come First-Served.
>
> *  values from 65000 to 65535 are marked as Private Use.
>
> [and reserve 147->162 in the table]
>
> Or:
>    *  values from 0 to 147 are marked as Specification Required.
>    *  values from 163 to 32767 are marked as Specification Required.
>    *  values from 32768 to 65000 are marked as First-Come First-
> Served.
>    *  values from 147 to 162, and 65000 to 65535 are marked as Private
> Use.
>
> Or:
>    *  values from 0 to 32767 (except 142 to 162) are marked as
> Specification Required.
>    *  values from 32768 to 65000 are marked as First-Come First-
> Served.
>    *  values from 147 to 162, and 65000 to 65535 are marked as Private
> Use.

I think this last one might be the way to go, but if you wanted to present
this information only as a table, we'll be using this style, minus the hex,
in the registry (with "Private Use" in the registration procedure table and
"Reserved for Private Use" in the registry itself):

https://www.iana.org/assignments/bgp-well-known-communities/bgp-well-known-communities.xhtml#bgp-well-known-communities-1

--
Michael Richardson <mcr+IETF at sandelman.ca>   . o O ( IPv6 IøT consulting )
           Sandelman Software Works Inc, Ottawa and Worldwide
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 487 bytes
Desc: not available
URL: <http://www.winpcap.org/pipermail/pcap-ng-format/attachments/20210106/f38fb558/attachment.sig>

From mcr+ietf at sandelman.ca  Thu Jan  7 00:05:27 2021
From: mcr+ietf at sandelman.ca (Michael Richardson)
Date: Wed, 06 Jan 2021 19:05:27 -0500
Subject: [pcap-ng-format] about the large initial registry table for
	gharris-opsawg-pcap
In-Reply-To: <5632.1609977060@localhost>
References: <21576.1609796505@localhost>
 <D3A753C3-ACCA-40B0-8028-E003DB29BF8A@sonic.net> <5632.1609977060@localhost>
Message-ID: <9100.1609977927@localhost>


Michael Richardson <mcr+ietf at sandelman.ca> wrote:
    > <record date="yyyy-mm-dd">
    > <value>0</value>
    > <name>LINKTYPE_NULL</name>
    > <description>BSD loopback encapsulation</description>
    > <xref type="draft" data="draft-gharris-opsawg-pcap-01"/>
    > </record>
    > [plus more details]

Guy asked in response to this:

> Do we need the symbolic name?  Those may become part of a future API in
> libpcap that provides raw pcapng blocks - or we might just map them to DLT_
> values, but, in either case, are they something that needs to be published
> in the registry?  That might be in the documentation for libpcap, instead,
> giving a reference to the registry and a list of numerical values,
> LINKTYPE_ names, and DLT_ names.

> Note that Wireshark does *not* use LINKTYPE_ names, it uses numerical
> values; the reason for introducing the LINKTYPE_ values was to work around
> platform differences in DLT_ values, so that a given link-layer type would
> always have the same numerical value in files from all platforms.

To which I had to say: "huh, I dunno."
I sort of think that having them means that different code might have the
same symbolic name in it, and that will improve things.

> The closest equivalent to this registry would be the equivalent one for Sun Snoop format:

> 	https://www.iana.org/assignments/snoop-datalink-types/snoop-datalink-types.xhtml

> That registry just gives a numerical value, a very brief description, and a
> reference to something that gives more details.

> Presumably most if not all references would be to RFCs; those would have to
> give as much detail as we have in the tcpdump.org registry:

> 	https://www.tcpdump.org/linktypes.html

When I went through the list and edited things a bit, I saw these kinds of things:

1) It's some document or source code over THERE.
2) It's RFCxyz format. (PERIOD!)
3) It's RFCxyz format... qualfier, qualifer, qualifier.
4) Three sentences that reference six documents that might be behind paywalls.

For (3), should RFCxyz ever get revised, it would be nice if it clarified the format.
In 95% of the time, the people who use that linktype know (knew?) the format
intimately, but it's unlikely that it's still that active.

Lest anyone think that those entries make this list a waste of time, we
register a few link types each year, often for things which are surprising.
USB captures were the first of many such surprises.

There are dissectors out there that can, for instance, take HTTP headers out of
HTTP/IP/ethernet/**USB** captures...

Conversely many 802.15.4 captures come with an extra layer of UDP/IP (in
LINKTYPE_ETHERNET) because a particular capture device did that.

--
Michael Richardson <mcr+IETF at sandelman.ca>   . o O ( IPv6 IøT consulting )
           Sandelman Software Works Inc, Ottawa and Worldwide
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 487 bytes
Desc: not available
URL: <http://www.winpcap.org/pipermail/pcap-ng-format/attachments/20210106/c0f202e4/attachment.sig>

