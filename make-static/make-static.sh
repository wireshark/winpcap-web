#!/bin/bash


while read -r php_file ; do
    save_file="old-php-content${php_file#site}"
    echo "$php_file $save_file"
    mkdir -p "$(dirname $save_file)"
    cp -f "$php_file" "$save_file" || exit
    php --php-ini ./php.ini -f "$save_file" > "$php_file"
done < <( grep -irl '<\?php *include' site )